﻿using Microsoft.EntityFrameworkCore;

namespace Animals.Models
{
    public class AnimalDbContext : DbContext
    {
        public AnimalDbContext(DbContextOptions options) : base(options)
        {}

        public DbSet<Animal> Animals { get; set; }
    }
}