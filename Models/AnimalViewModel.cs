﻿using System.Collections.Generic;

namespace Animals.Models
{
    public class AnimalViewModel
    {
        public Animal Animal { get; set; }
        public List<Animal> Animals { get; set; }
    }
}