﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Animals.Models
{
    public static class SeedData
    {

        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context =
                new AnimalDbContext(serviceProvider.GetRequiredService<DbContextOptions<AnimalDbContext>>()))
            {
                if (context.Animals.Any())
                {
                    return;
                }
                
                context.Animals.AddRange(
                    new Animal
                    {
                        Name = "Puszek",
                        Type = "Cat",
                        Birthdate = DateTime.Parse("2018-01-02")
                    },
                    
                    new Animal
                    {
                        Name = "Jerry",
                        Type = "Mouse",
                        Birthdate = DateTime.Parse("2000-06-05")
                    }
                );

                context.SaveChanges();
            }
        }
        
    }
}