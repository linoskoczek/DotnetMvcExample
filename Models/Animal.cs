﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Animals.Models
{
    public class Animal
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        
        [Required(ErrorMessage = "Name is required")]
        [StringLength(60, MinimumLength = 2)]
        public string Name { get; set; }
        
        [Required(ErrorMessage = "Type is required")]
        [StringLength(60, MinimumLength = 3)]
        public string Type { get; set; }
        
        [Required(ErrorMessage = "Birthdate is required")]
        [DataType(DataType.Date)]
        public DateTime Birthdate { get; set; }
    }
}