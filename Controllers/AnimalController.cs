﻿using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Animals.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Animals.Controllers
{
    public class AnimalController : Controller
    {
        private readonly AnimalDbContext _context;
        
        public AnimalController(AnimalDbContext context)
        {
            _context = context;
        }

        // GET
        public IActionResult Index(Animal animal)
        {
            var animalViewModel = new AnimalViewModel
            {
                Animal = animal,
                Animals = _context.Animals.ToList()
            };
            return View(animalViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add([Bind("ID,Name,Type,Birthdate")] Animal animal)
        {
            if (ModelState.IsValid)
            {
                _context.Add(animal);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index", animal);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();
            
            var animal = await _context.Animals.SingleOrDefaultAsync(a => a.Id == id);
            
            if (animal == null)
                return NotFound();
            
            _context.Animals.Remove(animal);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}